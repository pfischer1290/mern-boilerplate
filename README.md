# MERN Boilerplate

Based on React starter project https://github.com/react-boilerplate/react-boilerplate

## Components

### Frontend
according to https://github.com/react-boilerplate/react-boilerplate

\+ added reactstrap as bootstrap wrapper

Folder /

### Backend
express-generator

FOLDER: /backend

### Run the app
_requires:_

`npm i -g nodemon`

* clone this project
* install dependencies `npm i && cd backend && npm i`
* start frontend and backend with one command `npm run serve` *OR*
* Start the frontend react app with `npm start` in project root
* Start the Backend Express Node App with `cd backend && PORT=3001 node bin/www` for now

Now you have two server instances running:
* Frontend via localhost:3000
* Backend via localhost:3001
* API is proxied from localhost:3000/api ~> localhost:3001/
* call API from frontend just via `/api`

### Todo

* [X] startup frontend and backend via npm script using concurrently
* [ ] startup frontend and backend via npm script using pm2
